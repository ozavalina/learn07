public class Book {
    public static int numberOfBooks;
    public static String something;

    String author;
    String bookTitle;

    public Book(String author, String bookTitle) {
        this.author = author;
        this.bookTitle = bookTitle;
    }

    public static void main(String[] args) {
        Book book1 = new Book("Александр Сергеевич Пушкин", "Руслан и Людмила");
        Book book2 = new Book("Александр Сергеевич Пушкин", "Руслан и Людмила");
        Book book3 = new Book("Аркадий и Борис Стругацкие", "Трудно быть богом");

        System.out.println(book1.equals(book2));
        System.out.println(book1.equals(book3));
        System.out.println(book3.equals(book2));

        System.out.println(book1.hashCode());
        System.out.println(book2.hashCode());
        System.out.println(book3.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj.getClass() != getClass()) return false;
        Book book = (Book) obj;
        return this.author.equals(book.author) && this.bookTitle.equals(book.bookTitle);
    }

    public int hashCode() {
        return this.author.length() + this.bookTitle.length();
    }
}
