import java.util.*;

public class TaskSevenOne {
    public static void main(String[] args) {
        //1. Создать массив цветов (минимум 7 позиций).
        String[] arrayOfColors = {"красный", "оранжевый", "жёлтый", "зеленый", "голубой", "синий", "фиолетовый"};

        //2. Сконвертировать массив в список (ArrayList).
        //2.1. Arrays.asList()
        List<String> asList = Arrays.asList(arrayOfColors);
        System.out.println("asList: " + asList);
        //2.2. Collections.addAll()
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, arrayOfColors);
        System.out.println("list: " + list);
        //2.3. Перебор
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < arrayOfColors.length; i++) {
            arrayList.add(arrayOfColors[i]);
        }
        System.out.println("arrayList: " + arrayList);

        //3. Вывести на экран весь список с помощью итератора.
        Iterator<String> iterator = arrayList.iterator();
        System.out.print("Вывод с помощью итератора: ");
        while (iterator.hasNext()) {
            String lc = iterator.next();
            System.out.print(lc + " ");
        }
        System.out.println(" ");

        //4. Отсортировать список по алфавиту.
        Collections.sort(arrayList);
        System.out.println("Отсортированный по алфавиту список: " + arrayList);

        //5. Заменить элементы списка на позициях 1, 3, 5 на аналогичные, но с приставкой + "Dark ".
        arrayList.set(0, "Dark " + arrayList.get(0));
        arrayList.set(2, "Dark " + arrayList.get(2));
        arrayList.set(4, "Dark " + arrayList.get(4));
        System.out.println(arrayList);

        //6. Вывести на экран весь список с помощью for-each цикла.
        System.out.print("Вывод с помощью for-each цикла: ");
        for (String al : arrayList) {
            System.out.print(al + " ");
        }
        System.out.println(" ");

        //7. Получить под-список с 1 по 5 элементы включительно.
        ArrayList<String> arrayList2 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            arrayList2.add(arrayList.get(i));
        }
        System.out.println("Подсписок arrayList с 1 по 5 элементы:  " + arrayList2);

        //8. Написать метод, который поменяет местами 1 и 4 элементы между собой.
        String el1 = arrayList.get(0);
        arrayList.set(0, arrayList.get(3));
        arrayList.set(3, el1);
        System.out.println("Поменяли местами 1 и 4 элементы:  " + arrayList);

        //9. Получить элемент с индексом 3, сохранить в переменную а1.
        String a1 = arrayList.get(3);

        //10. Проверить, содержится ли объект из переменной а1 в коллекции.
        System.out.println("Элемент содержится в коллекции (true/false): " + arrayList.contains(a1));

        //11. Удалить все элементы, содержащие букву 'o'
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            String lc = it.next();
            if (lc.contains("о")) {
                it.remove();
            }
        }
        System.out.println("После удаления слов, содержащих о: " + arrayList);

        //12. Превратить список в массив.
        String[] arrayOfColors2 = new String[4];
        arrayList.toArray(arrayOfColors2);
        System.out.println("Обратно в массив: " + Arrays.toString(arrayOfColors2));

        //13. Похвалить себя и налить чай с печенькой. Ты - молодец!
        System.out.println("УРА!");
    }

}
