import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ProductivityTest {
    static ArrayList<Integer> arrList = new ArrayList<>();
    static LinkedList<Integer> linkList = new LinkedList<>();

    public static void main(String[] args) {
        fillOutLists();


        long timeBefore = System.currentTimeMillis(); //запомнили время до выполнения операции

        //calculateImportantNumbers(); //!!! операция, время работы которой мы оцениваем Время выполнения операции: 1783 миллисекунд

        //addElementToBeginningOfList(arrList); // Время выполнения операции: 1 миллисекунд
        //addElementToBeginningOfList(linkList); // Время выполнения операции: 0 миллисекунд
        //printElementsFromMiddleOfList(arrList); // Время выполнения операции: 1 миллисекунд
        //printElementsFromMiddleOfList(linkList); // Время выполнения операции: 215 миллисекунд
        //removeFirstElementsOfList(arrList); // Время выполнения операции: 235 миллисекунд
        //removeFirstElementsOfList(linkList); // Время выполнения операции: 13 миллисекунд
        //setElements(arrList); // Время выполнения операции: 0 миллисекунд
        //setElements(linkList); // Время выполнения операции: 205 миллисекунд


        /*Вывод*/
        //LinkedList - операция удаления элемента значительно быстрее, чем на ArrayList
        //Операция добавления элемента +- одинаково по скорости у LinkedList и у ArrayList, LinkedList чуть быстрее
        //Операции получения элемента и установки нового значения элемента на LinkedList значительно медленнее происходят, чем у ArrayList
        //Превышение по скорости операций у ArrayList связано с тем, что у него есть индекс элемента, по которому можно быстро найти элемент,
        //а у LinkedList ссылки на следующий и предыдущий элементы, соответственно, чтоб найти элемент в середине списка, необходимо по ссылкам дойти до него.
        //Операция удаления на LinkedList быстрее, чем на ArrayList, потому что при удалении элемента из ArrayList происходит перезапись всех последующих элементов
        //на количество удаленных элементов. LinkedList достаточно перезаписать ссылки у предыдущего и следующего элемента, чтоб они указывали друг на друга.

        long timeAfter = System.currentTimeMillis(); //запомнили время после выполнения операции

        long result = timeAfter - timeBefore; //получаем разницу во времени - реальное время выполнения операции
        System.out.println(String.format("Время выполнения операции: %d миллисекунд", result));
    }

    public static void fillOutLists() {
        for (int i = 0; i < 1000000; i++) {
            arrList.add(i);
            linkList.add(i);
        }
    }

    public static void addElementToBeginningOfList(List<Integer> list) {
        list.add(0, 12345);
    }

    public static void printElementsFromMiddleOfList(List<Integer> list) {
        for (int i = 500000; i < 500100; i++) {
            System.out.println(list.get(i));
        }
    }

    public static void removeFirstElementsOfList(List<Integer> list) {
        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            Integer lc = it.next();
            if (lc < 1000) {
                it.remove();
            }
        }
    }

    public static void setElements(List<Integer> list) {
        for (int i = 500000; i < 500100; i++) {
            list.set(i, i / 100);
        }
    }

    public static void calculateImportantNumbers() {
        for (int i = 0; i < 1000000; i++) {
            System.out.println("Теперь i = " + i);
        }
    }
}
